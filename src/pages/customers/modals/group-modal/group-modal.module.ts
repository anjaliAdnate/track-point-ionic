import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupModalPage } from './group-modal';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    GroupModalPage
  ],
  imports: [
    IonicPageModule.forChild(GroupModalPage),
    TranslateModule.forChild()
  ]
})
export class GroupModalPageModule {}
