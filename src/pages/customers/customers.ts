import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Events, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';
import { ReportSettingModal } from './modals/report-setting/report-setting';

@IonicPage()
@Component({
  selector: 'page-customers',
  templateUrl: 'customers.html',
})
export class CustomersPage implements OnInit {
  islogin: any;
  setsmsforotp: any;
  isSuperAdminStatus: any;
  customerslist: any;
  CustomerArray: any;
  CustomerArraySearch: any = [];

  CratedeOn: string;
  CustomerData: any;
  time: string;
  date: string;
  customer: any;
  customersignups: any;
  DeletedDevice: any;
  viewCtrl: any;
  isDealer: boolean;
  expirydate: string;
  page: number = 1;
  limit: number = 5;
  ndata: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alerCtrl: AlertController,
    public events: Events,
    private translate: TranslateService
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("_id=> " + this.islogin._id);
    this.setsmsforotp = localStorage.getItem('setsms');
    this.isSuperAdminStatus = this.islogin.isSuperAdmin;
    this.isDealer = this.islogin.isDealer;
  }

  ngOnInit() {
    this.getcustomer();
  }

  reportSetting(item) {
    let modal = this.modalCtrl.create(ReportSettingModal, {
      param: item
    });
    modal.onDidDismiss((data) => {
      this.navCtrl.setRoot("CustomersPage")
    });
    modal.present();
  }

  dialNumber(number) {
    window.open('tel:' + number, '_system');
  }

  doRefresh(refresher) {
    this.getcustomer();
    refresher.complete();
  }

  getItems(ev: any) {
    const val = ev.target.value.trim();
    this.CustomerArraySearch = this.CustomerArray.filter((item) => {
      return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
    console.log("search====", this.CustomerArraySearch);
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;
    setTimeout(() => {
      var baseURLp;
      baseURLp = this.apiCall.mainUrl + 'users/getCust?uid=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit;
      that.ndata = [];
      this.apiCall.getCustomersCall(baseURLp)
        .subscribe(data => {
          that.ndata = data;

          for (let i = 0; i < that.ndata.length; i++) {
            that.CustomerData.push(that.ndata[i]);
          }

          that.CustomerArraySearch = that.CustomerData;
        },
          err => {
            this.apiCall.stopLoading();
          });

      infiniteScroll.complete();
    }, 500);
  }

  callSearch(ev) {
    var searchKey = ev.target.value;
    this.page = 1;
    this.apiCall.callcustomerSearchService(this.islogin._id, this.page, this.limit, searchKey)
      .subscribe(data => {
        this.CustomerArraySearch = data;
      },
        err => {
          var msg = JSON.parse(err._body);
          let toast = this.toastCtrl.create({
            message: msg.message,
            duration: 2000,
            position: 'bottom'
          });
          toast.present();
        })
  }

  onClear(ev) {
    this.getcustomer();
    ev.target.value = '';
  }

  getcustomer() {
    var baseURLp = this.apiCall.mainUrl + 'users/getCust?uid=' + this.islogin._id + '&pageNo=' + this.page + '&size=' + this.limit;
    this.apiCall.startLoading().present();
    this.apiCall.getCustomersCall(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.CustomerData = data;
        this.CustomerArraySearch = this.CustomerData;

      },
        err => {
          this.apiCall.stopLoading();
          var a = JSON.parse(err._body);
          var b = a.message;
          let toast = this.toastCtrl.create({
            message: b,
            duration: 2000,
            position: "bottom"
          })
          toast.present();
          // toast.onDidDismiss(() => {
          //   this.navCtrl.setRoot('DashboardPage');
          // });
        });
  }

  CustomerStatus(Customersdeta) {
    var msg;
    if (Customersdeta.status) {
      msg = this.translate.instant('deactivateDealer', { value: this.translate.instant('cust') });
    } else {
      msg = this.translate.instant('activateDealer', { value: this.translate.instant('cust') });
    }
    let alert = this.alerCtrl.create({
      message: msg,
      buttons: [{
        text: this.translate.instant('Yes'),
        handler: () => {
          this.user_status(Customersdeta);
        }
      },
      {
        text: this.translate.instant('NO'),
        handler: () => {
          this.getcustomer();
        }
      }]
    });
    alert.present();
  }

  user_status(Customersdeta) {
    var stat;
    if (Customersdeta.status) {
      stat = false;
    } else {
      stat = true;
    }

    var data = {
      "uId": Customersdeta._id,
      "loggedIn_id": this.islogin._id,
      "status": stat
    };
    this.apiCall.startLoading().present();
    this.apiCall.user_statusCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.DeletedDevice = data;
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Customer updated!!'),
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          this.getcustomer();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  openupdateCustomersModal(Customersdetails) {
    this.customer = Customersdetails;
    let modal = this.modalCtrl.create('UpdateCustModalPage', {
      param: this.customer
    });
    modal.onDidDismiss((data) => {
      this.navCtrl.setRoot("CustomersPage")
    });
    modal.present();
  }

  openAdddeviceModal(Customersdetails) {
    this.customer = Customersdetails;
    let profileModal = this.modalCtrl.create('AddDeviceModalPage', { custDet: this.customer });
    profileModal.onDidDismiss(data => {
      this.getcustomer();
    });
    profileModal.present();
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

  openAddCustomerModal() {
    let modal = this.modalCtrl.create('AddCustomerModal');
    modal.onDidDismiss(() => {
      this.getcustomer();
    })
    modal.present();
  }

  switchUser(cust_id) {
    localStorage.setItem('isDealervalue', 'true');
    localStorage.setItem('dealer', JSON.stringify(this.islogin));
    localStorage.setItem('custumer_status', 'ON');
    localStorage.setItem('dealer_status', 'OFF');

    this.apiCall.getcustToken(cust_id)
      .subscribe(res => {
        var custToken = res;
        var logindata = JSON.stringify(custToken);
        var logindetails = JSON.parse(logindata);
        var userDetails = window.atob(logindetails.custumer_token.split('.')[1]);

        var details = JSON.parse(userDetails);
        localStorage.setItem("loginflag", "loginflag");
        localStorage.setItem('details', JSON.stringify(details));

        var dealerSwitchObj = {
          "logindata": logindata,
          "details": userDetails,
          'condition_chk': details.isDealer
        }

        var temp = localStorage.getItem('isDealervalue');
        this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
        this.events.publish("sidemenu:event", temp);
        this.navCtrl.setRoot('DashboardPage');

      }, err => {
        console.log(err);
      })
  }

  DelateCustomer(_id) {
    let alert = this.alerCtrl.create({
      message: this.translate.instant('Do you want to delete this customer?'),
      buttons: [{
        text: this.translate.instant('NO')
      },
      {
        text: this.translate.instant('Yes'),
        handler: () => {
          this.deleteCus(_id);
        }
      }]
    });
    alert.present();
  }


  deleteCus(_id) {
    var data = {
      "userId": _id,
      'deleteuser': true
    }
    this.apiCall.deleteCustomerCall(data).
      subscribe(data => {
        let toast = this.toastCtrl.create({
          message: this.translate.instant("Deleted successfully!!"),
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          this.getcustomer();
        });

        toast.present();
      },
        err => {
          console.log(err);
        });
  }
}
