import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController, Events } from "ionic-angular";
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from "@ngx-translate/core";

@IonicPage()
@Component({
    selector: 'page-customers',
    templateUrl: './dealers.html'
})

export class DealerPage {

    islogin: any = {};
    page: number = 1;
    limit: number = 5;
    DealerArraySearch: any = [];
    DealerArray: any = [];
    ndata: any = [];
    DealerData: any = [];
    CratedeOn: string;
    expirydate: string;
    time: string;
    date: string;
    searchKey: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiCall: ApiServiceProvider,
        public modalCtrl: ModalController,
        public toastCtrl: ToastController,
        public alerCtrl: AlertController,
        public events: Events,
        private translate: TranslateService
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }

    ionViewDidLoad() {
        this.getDealersListCall();
    }

    getDealersListCall() {
        this.apiCall.startLoading().present();
        this.apiCall.getDealersCall(this.islogin._id, this.page, this.limit, this.searchKey)
            .subscribe(data => {
                this.apiCall.stopLoading();
                this.DealerArray = data;
                this.DealerArraySearch = data;
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log("getting error from server=> ", err);
                    // var s = JSON.parse(err._body);
                    // var p = s.message;
                    let toast = this.toastCtrl.create({
                        message: this.translate.instant('No Dealer(s) found'),
                        duration: 2000,
                        position: "bottom"
                    })

                    toast.present();

                    toast.onDidDismiss(() => {
                        this.navCtrl.setRoot("DashboardPage");
                    })
                });
    }

    addDealersModal() {
        let modal = this.modalCtrl.create('AddDealerPage');
        modal.onDidDismiss(() => {
            this.getDealersListCall();
        })
        modal.present();
    }

    _editDealer(item) {
        let modal = this.modalCtrl.create('EditDealerPage', {
            param: item
        });
        modal.onDidDismiss(() => {
            this.getDealersListCall();
        })
        modal.present();
    }

    DeleteDealer(_id) {
        let alert = this.alerCtrl.create({
            message: this.translate.instant('doyouwanttodeletedealer', {value: this.translate.instant('Dealers')}),
            buttons: [{
                text: this.translate.instant('NO')
            },
            {
                text: this.translate.instant('Yes'),
                handler: () => {
                    this.deleteDeal(_id);
                }
            }]
        });
        alert.present();
    }

    deleteDeal(_id) {
        var deletePayload = {
            'userId': _id,
            'deleteuser': true
        }
        this.apiCall.startLoading().present();
        this.apiCall.deleteDealerCall(deletePayload).
            subscribe(data => {
                this.apiCall.stopLoading();
                let toast = this.toastCtrl.create({
                    message: this.translate.instant("deletedDealerSucc", {value: this.translate.instant('dealers')}),
                    position: 'bottom',
                    duration: 2000
                });

                toast.onDidDismiss(() => {
                    this.getDealersListCall();
                });

                toast.present();
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log(err);
                });
    }

    callSearch(ev) {
        this.searchKey = ev.target.value;
        this.apiCall.getDealersCall(this.islogin._id, this.page, this.limit, this.searchKey)
            .subscribe(data => {
                this.DealerArraySearch = data;
                this.DealerArray = data;
            }, err => {
                // var a = JSON.parse(err._body);
                // var b = a.message;
                this.DealerArraySearch = [];
                this.DealerArray = [];
                let toast = this.toastCtrl.create({
                    message: this.translate.instant("No Dealer found for search key '") + ev.target.value + "' ..",
                    duration: 1500,
                    position: "bottom"
                })
                toast.present();
                toast.onDidDismiss(() => { })
            })
    }

    onClear(ev) {
        this.getDealersListCall();
        ev.target.value = '';
    }

    switchDealer(_id) {
        localStorage.setItem('superAdminData', JSON.stringify(this.islogin));
        localStorage.setItem('custumer_status', 'OFF');
        localStorage.setItem('dealer_status', 'ON');

        this.apiCall.getcustToken(_id)
            .subscribe(res => {
                var custToken = res;
                var logindata = JSON.stringify(custToken);
                var logindetails = JSON.parse(logindata);
                var userDetails = window.atob(logindetails.custumer_token.split('.')[1]);
                var details = JSON.parse(userDetails);
                localStorage.setItem("loginflag", "loginflag");
                localStorage.setItem('details', JSON.stringify(details));

                var dealerSwitchObj = {
                    "logindata": logindata,
                    "details": userDetails,
                    'condition_chk': details.isDealer
                }

                var temp = localStorage.getItem('isDealervalue');
                this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
                this.events.publish("sidemenu:event", temp);
                this.navCtrl.setRoot('DashboardPage');

            }, err => {
                console.log(err);
            })
    }

    dealerStatus(data) {
        var msg;
        if (data.status) {
            msg = this.translate.instant('deactivateDealer',{value: this.translate.instant('Dealers')});
        } else {
            msg = this.translate.instant('deactivateDealer',{value: this.translate.instant('Dealers')});
        }
        let alert = this.alerCtrl.create({
            message: msg,
            buttons: [{
                text: this.translate.instant('Yes'),
                handler: () => {
                    this.user_status(data);
                }
            },
            {
                text: this.translate.instant('NO'),
                handler: () => {
                    this.getDealersListCall();
                }
            }]
        });
        alert.present();
    }

    user_status(data) {
        var stat;
        if (data.status) {
            stat = false;
        } else {
            stat = true;
        }

        var ddata = {
            "uId": data._id,
            "loggedIn_id": this.islogin._id,
            "status": stat
        };
        this.apiCall.startLoading().present();
        this.apiCall.user_statusCall(ddata)
            .subscribe(data => {
                this.apiCall.stopLoading();
                let toast = this.toastCtrl.create({
                    message: this.translate.instant('Dealer status updated successfully!'),
                    position: 'bottom',
                    duration: 2000
                });

                toast.onDidDismiss(() => {
                    this.getDealersListCall();
                });

                toast.present();
            },
                err => {
                    this.apiCall.stopLoading();
                });
    }

    doInfinite(infiniteScroll) {
        let that = this;
        that.page = that.page + 1;

        setTimeout(() => {
            that.ndata = [];
            this.apiCall.getDealersCall(that.islogin._id, that.page, that.limit, that.searchKey)
                .subscribe(data => {
                    that.ndata = data;

                    for (let i = 0; i < that.ndata.length; i++) {
                        that.DealerData.push(that.ndata[i]);
                    }
                    that.DealerArraySearch = that.DealerData;
                    infiniteScroll.complete();
                },
                    err => {
                        this.apiCall.stopLoading();
                        console.log("error found=> " + err);
                    });
        }, 500);
    }
}
