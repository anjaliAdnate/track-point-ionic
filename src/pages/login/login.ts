import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, PopoverController, ViewController, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;
  submitAttempt: boolean;
  otpMess: any;
  data: {};
  logindata: any;
  userDetails: string;
  details: any;
  showPassword: boolean = false;
  languages: any;

  constructor(

    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public apiservice: ApiServiceProvider,
    public toastCtrl: ToastController,
    public popoverCtrl: PopoverController,
    public translate: TranslateService,

  ) {

    this.loginForm = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })

  }

  // presentPopover() {
  //   let popover = this.popoverCtrl.create(LanguagesPage, {}, { enableBackdropDismiss: false });
  //   // popover.onDidDismiss(() => {
  //   //   var lng = localStorage.getItem("LANG_KEY");
  //   //   this.translate.use(lng);
  //   // })
  //   popover.present({});
  // }

  ionViewWillEnter() {
    console.log("ionViewWillEnter")
    // if (localStorage.getItem("LANG_KEY") == null) {
    //   this.presentPopover();
    // }
  }

  ionViewDidEnter() {
    console.log("ionViewDidEnter")
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');

    // this.presentPopover()
    // let alert = this.alertCtrl.create({
    //   message: "Select languages"
    // })
    // alert.present();
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  userlogin() {
    this.submitAttempt = true;

    if (this.loginForm.value.username == "") {
      /*alert("invalid");*/
      return false;
    }
    else if (this.loginForm.value.password == "") {
      /*alert("invalid");*/
      return false;
    }

    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    var isEmail = validateEmail(this.loginForm.value.username);
    var isName = isNaN(this.loginForm.value.username);
    var strNum;


    if (isName == false) {
      strNum = this.loginForm.value.username.trim();
    }

    if (isEmail == false && isName == false && strNum.length == 10) {

      this.data = {

        "psd": this.loginForm.value.password,
        "ph_num": this.loginForm.value.username
      };
    }
    else if (isEmail) {

      this.data = {

        "psd": this.loginForm.value.password,
        "emailid": this.loginForm.value.username
      };
    }
    else {

      this.data = {

        "psd": this.loginForm.value.password,
        "user_id": this.loginForm.value.username
      };
    }

    this.apiservice.startLoading();
    this.apiservice.loginApi(this.data)
      .subscribe(response => {
        this.logindata = response;
        this.logindata = JSON.stringify(response);
        var logindetails = JSON.parse(this.logindata);
        this.userDetails = window.atob(logindetails.token.split('.')[1]);
        this.details = JSON.parse(this.userDetails);
        console.log(this.details.email);
        localStorage.setItem("loginflag", "loginflag");
        localStorage.setItem('details', JSON.stringify(this.details));
        localStorage.setItem('condition_chk', this.details.isDealer);
        localStorage.setItem('VeryFirstLoginUser', this.details._id)
        localStorage.setItem("INTRO", "INTRO")

        this.apiservice.stopLoading();

        let toast = this.toastCtrl.create({
          message: this.translate.instant("Welcome! You're logged In successfully."),
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.navCtrl.setRoot('DashboardPage');
        });

        toast.present();
      },
        error => {
          // console.log("login error => "+error)
          var body = error._body;
          var msg = JSON.parse(body);
          if (msg.message == "Mobile Phone Not Verified") {
            let confirmPopup = this.alertCtrl.create({
              title: 'Login failed!',
              message: msg.message = !msg.message ? '  Do you Want to verify it?' : msg.message + '  Do you Want to verify it?',
              buttons: [
                { text: 'Cancel' },
                {
                  text: this.translate.instant('Okay'),
                  handler: () => {
                    // this.navCtrl.push(MobileVerify);
                  }
                }
              ]
            });
            confirmPopup.present();
          } else {
            // Do something on error
            let alertPopup = this.alertCtrl.create({
              title: 'Login failed!',
              message: msg.message,
              buttons: [this.translate.instant('Okay')]
            });
            alertPopup.present();
          }
          this.apiservice.stopLoading();
        });
  }

  presentToast(msg) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 2500
    });
    toast.present();
  }

  gotosignuppage() {
    this.navCtrl.push('SignupPage');
  }

  forgotPassFunc() {

    const prompt = this.alertCtrl.create({
      title: this.translate.instant('Forgot Password'),
      message: this.translate.instant('Enter your Registered Mobile Number and we will send you a confirmation code.'),
      inputs: [
        {
          name: 'mobno',
          placeholder: this.translate.instant('Mobile Number')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('SEND CONFIRMATION CODE'),
          handler: (data) => {
            var forgotdata = {
              "cred": data.mobno
            }
            this.apiservice.startLoading();
            this.apiservice.forgotPassApi(forgotdata)
              .subscribe(data => {
                this.apiservice.stopLoading();
                this.presentToast(data.message);
                this.otpMess = data;
                this.PassWordConfirmPopup();
              },
                error => {
                  this.apiservice.stopLoading();
                  var body = error._body;
                  var msg = JSON.parse(body);
                  let alert = this.alertCtrl.create({
                    title: this.translate.instant('Forgot Password Failed!'),
                    message: msg.message,
                    buttons: [this.translate.instant('Okay')]
                  });
                  alert.present();
                })
          }
        }
      ]
    });
    prompt.present();
  }

  PassWordConfirmPopup() {
    const prompt = this.alertCtrl.create({
      title: this.translate.instant('Reset Password'),
      // message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'newpass',
          placeholder: this.translate.instant('Password')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('Back'),
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('SAVE'),
          handler: data => {
            if (!data.newpass || !data.mobilenum || !data.confCode) {
              let alertPopup = this.alertCtrl.create({
                title: this.translate.instant('Warning'),
                message: this.translate.instant('Fill all mandatory fields!'),
                buttons: [this.translate.instant('Okay')]
              });
              alertPopup.present();
            } else {
              if (data.newpass == data.mobilenum && data.newpass && data.mobilenum) {
                if (data.newpass.length < 6 || data.newpass.length > 12) {
                  var Popup = this.alertCtrl.create({
                    title: this.translate.instant('Warning'),
                    message: this.translate.instant('Password length should be 6 - 12'),
                    buttons: [this.translate.instant('Okay')]
                  });
                  Popup.present();
                } else {
                  var Passwordset = {
                    "newpwd": data.newpass,
                    "otp": data.confCode,
                    "phone": this.otpMess,
                    "cred": this.otpMess
                  }
                  this.apiservice.startLoading();
                  this.apiservice.forgotPassMobApi(Passwordset)
                    .subscribe(data => {
                      this.apiservice.stopLoading();
                      this.presentToast(data.message);
                      this.navCtrl.setRoot(LoginPage);
                    },
                      error => {
                        this.apiservice.stopLoading();
                        var body = error._body;
                        var msg = JSON.parse(body);
                        let alert = this.alertCtrl.create({
                          title: this.translate.instant('Forgot Password Failed!'),
                          message: msg.message,
                          buttons: [this.translate.instant('Okay')]
                        });
                        alert.present();
                      });
                }
              } else {
                let alertPopup = this.alertCtrl.create({
                  title: this.translate.instant('Warning'),
                  message: this.translate.instant('New Password and Confirm Password Not Matched'),
                  buttons: [this.translate.instant('Okay')]
                });
                alertPopup.present();
              }
              if (!data.newpass || !data.mobilenum || !data.confCode) {
                //don't allow the user to close unless he enters model...
                return false;
              }
            }
          }
        }
      ]
    });
    prompt.present();
  }
}

@Component({
  template: `
    <ion-list>
      <ion-list-header style="text-align: center;">{{'Select Language' | translate}}</ion-list-header>
      <button ion-item (click)="setLanguage('en')">{{'English' | translate}}</button>
      <button ion-item (click)="setLanguage('hi')">{{'Hindi' | translate}}</button>
      <button ion-item (click)="setLanguage('bn')">{{'Bangali' | translate}}</button>
      <button ion-item (click)="setLanguage('te')">{{'Telugu' | translate}}</button>
      <button ion-item (click)="setLanguage('ta')">{{'Tamil' | translate}}</button>
      <button ion-item (click)="setLanguage('gu')">{{'Gujarati' | translate}}</button>
      <button ion-item (click)="setLanguage('kn')">{{'Kannada' | translate}}</button>
      <button ion-item (click)="setLanguage('mr')">{{'Marathi' | translate}}</button>
      <button ion-item (click)="setLanguage('ml')">{{'Malayalam' | translate}}</button>
      <button ion-item (click)="setLanguage('sp')">{{'Spanish' | translate}}</button>
      <button ion-item (click)="setLanguage('fa')">{{'Persian' | translate}}</button>
      <button ion-item (click)="setLanguage('ar')">{{'Arabic' | translate}}</button>
    </ion-list>
  `
})
export class LanguagesPage {
  islogin: any;
  constructor(
    public viewCtrl: ViewController,
    public translate: TranslateService,
    private apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private events: Events) {
      this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  close() {
    this.viewCtrl.dismiss();
  }

  setLanguage(key) {
    var payload = {
      uid: this.islogin._id,
      lang: key
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
      .subscribe((resp) => {
        console.log('response language code ' + resp);
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: resp.message,
          duration: 1500,
          position: 'bottom'
        });
        toast.present();
      });
    this.events.publish('lang:key', key);
    // this language will be used as a fallback when a translation isn't found in the current language
    // this.translate.use(key);
    // localStorage.setItem("LANG_KEY", key);
    this.viewCtrl.dismiss();
  }
}
