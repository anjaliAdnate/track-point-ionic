import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DistanceReportPage } from './distance-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';

@NgModule({
  declarations: [
    DistanceReportPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(DistanceReportPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
  exports: [
    OnCreate
  ]
})
export class DistanceReportPageModule {}
