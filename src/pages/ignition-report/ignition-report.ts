import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-ignition-report',
  templateUrl: 'ignition-report.html',
})
export class IgnitionReportPage implements OnInit {

  islogin: any;
  Ignitiondevice_id: any;
  igiReport: any[] = [];

  datetimeEnd: any;
  datetimeStart: string;
  devices: any;
  isdevice: string;
  portstemp: any;
  datetime: any;
  igiReportData: any[] = [];
  locationEndAddress: any;
  selectedVehicle: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalligi: ApiServiceProvider,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = this.apicalligi.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalligi.startLoading().present();
    this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });
  }

  getIgnitiondevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.Ignitiondevice_id = selectedVehicle.Device_Name;
  }

  getIgnitiondeviceReport() {

    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";

    }
    let that = this;
    this.apicalligi.startLoading().present();

    this.apicalligi.getIgiApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.igiReport = data;
        if (this.igiReport.length > 0) {
          this.innerFunc(this.igiReport);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected Dates/Vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }


      }, error => {
        this.apicalligi.stopLoading();
        console.log(error);
      })
  }

  innerFunc(igiReport) {
    let outerthis = this;
    outerthis.igiReportData = [];
    outerthis.locationEndAddress = undefined;
    var i = 0, howManyTimes = igiReport.length;
    function f() {

      outerthis.igiReportData.push({
        'vehicleName': outerthis.igiReport[i].vehicleName,
        'switch': outerthis.igiReport[i].switch,
        'timestamp': outerthis.igiReport[i].timestamp,
        'start_location': {
          'lat': outerthis.igiReport[i].lat,
          'long': outerthis.igiReport[i].long
        }
      });
      outerthis.start_address(outerthis.igiReport[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }

  start_address(item, index) {
    let that = this;
    that.igiReportData[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.igiReportData[index].StartLocation = "N/A";
    } else if (item.start_location) {
      this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
        .then((res) => {
          console.log("test", res)
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
          that.igiReportData[index].StartLocation = str;
        })
    }
  }
  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicalligi.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

}
