import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-device-summary-repo',
  templateUrl: 'device-summary-repo.html',
})
export class DeviceSummaryRepoPage implements OnInit {

  islogin: any;
  devices: any;
  portstemp: any;
  datetimeStart: string;
  datetimeEnd: string;
  device_id: any;
  summaryReport: any[] = [];
  locationEndAddress: any;
  locationAddress: any;
  datetime: number;
  selectedVehicle: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apicallsummary: ApiServiceProvider, public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    // var yestDay = moment().subtract(1, 'days');
    // console.log("yesterdays date: ", yestDay);
    console.log("yest time: ", moment({ hours: 0 }).subtract(1, 'days').format())
    // this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeStart = moment({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
    console.log("today time: ", this.datetimeStart)
    // this.datetimeEnd = moment().format();//new Date(a).toISOString();
    this.datetimeEnd = moment({ hours: 0 }).format(); // today date and time with 12:00am

  }

  ngOnInit() {
    this.getdevices();
  }

  getSummaarydevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.device_id = selectedVehicle.Device_ID;
  }

  getdevices() {

    var baseURLp = this.apicallsummary.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicallsummary.startLoading().present();
    this.apicallsummary.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicallsummary.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicallsummary.stopLoading();
          console.log(err);
        });
  }

  summaryReportData = [];
  getSummaryReport() {
    let that = this;
    this.summaryReport = []
    this.summaryReportData = [];

    if (this.device_id == undefined) {
      this.device_id = "";

    }
    this.apicallsummary.startLoading().present();
    this.apicallsummary.getSummaryReportApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.islogin._id, this.device_id)
      .subscribe(data => {
        this.apicallsummary.stopLoading();
        this.summaryReport = data;
        console.log(data);
        if (this.summaryReport.length > 0) {

          this.innerFunc(this.summaryReport);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected dates/vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
      }, error => {
        this.apicallsummary.stopLoading();
        console.log(error);
      })
  }



  innerFunc(summaryReport) {
    let outerthis = this;
    var i = 0, howManyTimes = summaryReport.length;
    function f() {
      // console.log("conversion: ", Number(outerthis.summaryReport[i].devObj[0].Mileage))
      // var hourconversion = 2.7777778 / 10000000;
      outerthis.summaryReportData.push(
        {
          'Device_Name': summaryReport[i].devObj[0].Device_Name,
          'routeViolations': summaryReport[i].today_routeViolations,
          'overspeeds': outerthis.millisecondConversion(summaryReport[i].today_overspeeds),
          'ignOn': outerthis.millisecondConversion(summaryReport[i].today_running),
          'ignOff': outerthis.millisecondConversion(summaryReport[i].today_stopped),
          'distance': summaryReport[i].today_odo,
          'tripCount': summaryReport[i].today_trips,
          'mileage': ((summaryReport[i].devObj[0].Mileage == null) || (summaryReport[i].devObj[0].Mileage == undefined)) ? "NA" : (summaryReport[i].today_odo / parseFloat(summaryReport[i].devObj[0].Mileage)).toFixed(2),
          'end_location': summaryReport[i].end_location,
          'start_location': summaryReport[i].start_location,
          't_ofr': outerthis.millisecondConversion(summaryReport[i].t_ofr),
          't_idling': outerthis.millisecondConversion(summaryReport[i].t_idling)
        });

      outerthis.start_address(summaryReport[i], i);
      outerthis.end_address(summaryReport[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  millisecondConversion(duration) {
    var minutes = Math.floor((duration / (1000 * 60)) % 60);
    var hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
    hours = (hours < 10) ? 0 + hours : hours;
    minutes = (minutes < 10) ? 0 + minutes : minutes;
    return hours + ":" + minutes;
  }

  start_address(item, index) {
    let that = this;
    that.summaryReportData[index].StartLocation = "N/A";
    if (item.start_location == null || item.start_location == undefined) {
      that.summaryReportData[index].StartLocation = "N/A";
    } else if (item.start_location != null || item.end_location != undefined) {
      this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
        .then((res) => {
          console.log("test", res)
          console.log("check lat: "+ item.start_location.lat)
          console.log("check long: "+ item.start_location.long)
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
          that.summaryReportData[index].StartLocation = str;
        })
    }
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicallsummary.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

  end_address(item, index) {
    let that = this;
    that.summaryReportData[index].EndLocation = "N/A";
    if (item.end_location== null || item.end_location == undefined) {
      that.summaryReportData[index].EndLocation = "N/A";
    } else if (item.end_location != null || item.end_location != undefined) {
      this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
        .then((res) => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
          that.summaryReportData[index].EndLocation = str;
        })
    }
  }
}
