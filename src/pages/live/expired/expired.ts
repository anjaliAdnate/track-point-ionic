import { Component } from "@angular/core";
import { IonicPage, NavParams, ViewController, ToastController, NavController } from "ionic-angular";

@IonicPage()
@Component({
    selector: 'page-expired',
    templateUrl: './expired.html'
})

export class ExpiredPage {

    constructor(
        public params: NavParams,
        public viewCtrl: ViewController,
        public toastCtrl: ToastController,
        public navCtrl: NavController
    ) {

        
    }

    activate() {
        // this.navCtrl.push("AddDevicesPage");
        console.log("Redirect to paytm page")
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    
}