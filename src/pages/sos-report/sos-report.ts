import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-sos-report',
  templateUrl: 'sos-report.html',
})
export class SosReportPage {
  sos_id: any;
  islogin: any;
  datetimeStart: any;
  datetimeEnd: any;
  sosData: any;
  devices1243: any[];
  portstemp: any;
  devices: any;
  isdevice: string;
  pageNo: number = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicall: ApiServiceProvider,
    public toastCtrl: ToastController
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    this.getdevices();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SosReportPage');
  }

  getsosdevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.sos_id = selectedVehicle.Device_Name;
  }

  getdevices() {
    var baseURLp = this.apicall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicall.startLoading().present();
    this.apicall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicall.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
        this.getSOSReport();
      },
        err => {
          this.apicall.stopLoading();
          console.log(err);
        });
  }

  getSOS(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.sos_id = selectedVehicle.Device_ID;
  }

  getSOSReport() {
    var _url;

    // if (this.sos_id == undefined) {
    //   this.sos_id = "";
    //   _url = 'https://www.oneqlik.in/notifs/SOSReport?from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&_u=' + this.islogin._id;
    // } else {
    //   _url = 'https://www.oneqlik.in/notifs/SOSReport?from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&dev_id=' + this.sos_id + '&_u=' + this.islogin._id;
    // }
    _url = "https://www.oneqlik.in/notifs/statusReport";
    var payload = {};
    debugger
    if (this.sos_id == undefined) {
      payload = {
        "draw": 3,
        "columns": [
          {
            "data": "_id"
          },
          {
            "data": "device"
          },
          {
            "data": "vehicleName"
          },
          {
            "data": "timestamp"
          },
          {
            "data": "address"
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": this.pageNo,
        "length": 10,
        "search": {
          "value": "",
          "regex": false
        },
        "find": {
          "user": this.islogin._id,
          "type": "SOS",
          "timestamp": {
            "$gte": new Date(this.datetimeStart).toISOString(),
            "$lte": new Date(this.datetimeEnd).toISOString()
          }
        }
      }
    } else {
      payload = {
        "draw": 3,
        "columns": [
          {
            "data": "_id"
          },
          {
            "data": "device"
          },
          {
            "data": "vehicleName"
          },
          {
            "data": "timestamp"
          },
          {
            "data": "address"
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": this.pageNo,
        "length": 10,
        "search": {
          "value": "",
          "regex": false
        },
        "find": {
          "user": this.islogin._id,
          "type": "SOS",
          "device": this.sos_id,
          "timestamp": {
            "$gte": new Date(this.datetimeStart).toISOString(),
            "$lte": new Date(this.datetimeEnd).toISOString()
          }
        }
      }
    }

    this.apicall.startLoading().present();
    // this.apicall.getSOSReportAPI(_url)
    this.apicall.urlpasseswithdata(_url, payload)
      .subscribe(data => {
        this.apicall.stopLoading();
        this.sosData = data.data;
        if (this.sosData.length == 0) {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected Dates/Vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }

      }, error => {
        this.apicall.stopLoading();
        console.log(error);
      })
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.pageNo += 1;
    setTimeout(() => {
      that.innerFunc(infiniteScroll);
    }, 200)
  }

  innerFunc(infiniteScroll) {
    let that = this;
    var _url1 = "https://www.oneqlik.in/notifs/statusReport";
    var payload = {};
    if (that.sos_id == undefined) {
      that.sos_id = "";
      payload = {
        "draw": 3,
        "columns": [
          {
            "data": "_id"
          },
          {
            "data": "device"
          },
          {
            "data": "vehicleName"
          },
          {
            "data": "timestamp"
          },
          {
            "data": "address"
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": that.pageNo,
        "length": 10,
        "search": {
          "value": "",
          "regex": false
        },
        "find": {
          "user": that.islogin._id,
          "type": "SOS",
          "timestamp": {
            "$gte": new Date(that.datetimeStart).toISOString(),
            "$lte": new Date(that.datetimeEnd).toISOString()
          }
        }
      }
    } else {
      payload = {
        "draw": 3,
        "columns": [
          {
            "data": "_id"
          },
          {
            "data": "device"
          },
          {
            "data": "vehicleName"
          },
          {
            "data": "timestamp"
          },
          {
            "data": "address"
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": that.pageNo,
        "length": 10,
        "search": {
          "value": "",
          "regex": false
        },
        "find": {
          "user": that.islogin._id,
          "type": "SOS",
          "device": that.sos_id,
          "timestamp": {
            "$gte": new Date(that.datetimeStart).toISOString(),
            "$lte": new Date(that.datetimeEnd).toISOString()
          }
        }
      }
    }

    this.apicall.urlpasseswithdata(_url1, payload)
      .subscribe(data => {
        for (var i = 0; i < data.data.length; i++) {
          that.sosData.push(data.data[i])
        }
        console.log("data length: " + that.sosData.length);
        infiniteScroll.complete();
      }, error => {
        this.apicall.stopLoading();
        console.log(error);
        infiniteScroll.complete();
      })
  }
}
